import discord
import apiai
import json

from discord.ext import commands

### SET UP
# pip install discord.py
# pip install apiai

###

bot = commands.Bot(command_prefix="!", description="Tazmanian Bot")
TOKEN = ''
auth_ids = ["83033286221238272"]
cmd = "!"

#apiai
CLIENT_ACCESS_TOKEN = ''

#dont edit
ai = ""
self_mention = ""
shutdown = False


@bot.event
async def on_ready():
    global ai, CLIENT_ACCESS_TOKEN, self_mention
    print("We're logged in as " + bot.user.name)
    ai = apiai.ApiAI(CLIENT_ACCESS_TOKEN)
    self_mention = create_mention(bot.user.id)

@bot.event
async def on_message(message):
    author = message.author
    channel = message.channel
    global cmd, self_mention, shutdown

    if author == bot.user:
        return
    if message.content.startswith(cmd + "shutdown"):
        if any(author.id in s for s in auth_ids):
            shutdown = True
            try:
                await bot.delete_message(message)
            except Exception as e:
                print(e)
            await bot.close()
        return
    if message.content.startswith(cmd + "invite"):
        try:
            invite = await bot.create_invite(channel, max_age=86400)
            await bot.send_message(channel, invite.url)
        except Exception as e:
            print(e)
            await bot.send_message(channel, create_mention(author.id) + ", I wasn't able to create an invite :(")
        return
    if message.content.startswith(cmd):
        content = message.content[len(cmd):]
        response = get_dialogflow_response(author, content)
        if response != "":
            await bot.send_message(channel, response)
        return
    if message.content.startswith(self_mention):
        content = message.content[len(self_mention):]
        response = get_dialogflow_response(author, content)
        if response != "":
            await bot.send_message(channel, response)

def get_dialogflow_response(author, content):
    global ai, CLIENT_ACCESS_TOKEN
    request = ai.text_request()
    request.session_id = author.id
    request.query = content
    response = request.getresponse()
    json_response = json.loads(response.read())
    return json_response['result']['fulfillment']['speech']

def create_mention(user_id):
    return "<@" + str(user_id) + ">"
def gen_cmd(command):
    global cmd
    return "`" + cmd + command + "`"

while shutdown == False:
    bot.run(TOKEN)
